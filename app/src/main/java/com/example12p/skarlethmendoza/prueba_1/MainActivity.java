package com.example12p.skarlethmendoza.prueba_1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button buttonFC;
    Button buttonCF;
    EditText editTextM;
    TextView textViewR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonFC = (Button) findViewById(R.id.buttonFC);
        buttonCF = (Button) findViewById(R.id.buttonCF);
        editTextM = (EditText) findViewById(R.id.editTextM);
        textViewR = (TextView) findViewById(R.id.textViewR);

        buttonFC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float valor = Float.parseFloat(editTextM.getText().toString());
                float resultadoC = (float) ((valor - 32)/(1.8));
                textViewR.setText(String.valueOf(resultadoC));
            }
        });

        buttonCF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float valor = Float.parseFloat(editTextM.getText().toString());
                float resultadoF = (float) ((valor * 1.8) + 32);
                textViewR.setText(String.valueOf(resultadoF));
            }
        });
    }
}
